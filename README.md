# LessonDjango

## Start

Copy `.env.example` to `.env`

Install dependencies

```shell
poetry install
poetry shell

make migrate

poetry run python3 manage.py createsuperuser

make up-local
```

Open [http://localhost:8000/admin](http://localhost:8000/admin)

## Tests

```shell
poetry run pytest
```